package by.asb.traningprogect.hospital.exception;

public class EntityCreatedException extends Exception{
    public EntityCreatedException(String message) {
        super(message);
    }
}
