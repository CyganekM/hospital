package by.asb.traningprogect.hospital.model;

public enum FieldFilter {
    DOB,
    NAME,
    SURNAME,
    SECOND_NAME,
    ADDRESS,
    PHONE,
    EMAIL
}
