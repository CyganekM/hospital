package by.asb.traningprogect.hospital.model;

public enum QueryOperator {
    GREATER_THAN,
    LESS_THAN,
    EQUALS,
    LIKE,
    NOT_EQ,
    IN
}
