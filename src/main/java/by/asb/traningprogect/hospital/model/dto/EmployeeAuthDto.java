package by.asb.traningprogect.hospital.model.dto;

public record EmployeeAuthDto( String email, String password) {
}
