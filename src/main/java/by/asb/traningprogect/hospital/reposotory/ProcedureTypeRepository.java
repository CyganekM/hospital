package by.asb.traningprogect.hospital.reposotory;

import by.asb.traningprogect.hospital.model.entity.ProcedureType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcedureTypeRepository extends JpaRepository<ProcedureType, Integer> {
}