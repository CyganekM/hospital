package by.asb.traningprogect.hospital.reposotory;

import by.asb.traningprogect.hospital.model.entity.Specialization;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecializationRepository extends JpaRepository<Specialization, Integer> {
}