# Hospital

Система больница. Учебный проект IT-Academy 

## Features
 - Администратор заводит в БД работников назначает роли и устанавливает первоначальный пароль. Логин : "admin@company.com",
   пароль : "admin"
 - Врач определяет диагноз, делает назначения пациенту(процедуры, лекарства, операции). 
 - Назначения может выполнить медсестра(процедуры, лекарства).
 - Пациент может быть выписан из больницы, при этом фиксируется окончательный диагноз. 

## Technologies used in the project

- Java 21
- Mysql 8.3
- Spring:
    - SpringBoot
    - Data JPA
    - Security (JWT Token)
- Maven
- RestApi
- Test: JUnit, Mockito
- Lombok
- Swaqger
- FlyWay
- Docker compose

## Installation on the WINDOWS OS

### Установка JAVA. Можно пропустить, если установлена.

- Ссылка для загрузки [java 21](https://download.oracle.com/java/22/latest/jdk-22_windows-x64_bin.zip)  
- Разархивировать c:\jdk-21.0.3
- В системные переменные добавить:
     1) JAVA_HOME   путь c:\jdk-21.0.3;
     2) в Path добавить %JAVA_HOME\%bin.
- Перезагрузить ПК

### Установить Docker Desktop
-  Ссылка для загрузки [Docker Desktop](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe?utm_source=docker&utm_medium=webreferral&utm_campaign=dd-smartbutton&utm_location=module)

### Запуск приложения
- jar архив должен находится в одной директори с `docker-compose.yaml`
- в коммандной строке набрать `java -jar hospital-3.2.4.jar`(запускать из каталога в котором находится архив 
или указать полный путь к файлу)